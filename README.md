# Integração com SpaceX API

Este projeto foi criado par servir como front-end para integração com a API [SpaceX REST API](https://github.com/r-spacex/SpaceX-API).

## O projeto foi criado por template com o comando:

npx create-react-app spacex-front --template typescript

## Para iniciar a aplicação localmente:

### `npm start`

Roda o app em ambiente de desenvolvimento,

-   Vale lembrar que para iniciar qualquer aplicação nodejs primeiro se deve instalar as dependências com comandos como o `npm i` e somente após isso deve ser utilizado o comando `npm start`.

O servidor de desenvolvimento roda local na porta 3000 -> [http://localhost:3000](http://localhost:3000).

## Implementação:

O projeto foi criado sob a base de Typescript, pensado para evitar alto acoplamento e com muita componentização. A separação dos arquivos foi feita de modo que não fiquem arquivos grandes demais que podem acabar atrapalhando a manutenção do código e para melhorar a coesão.

O layout foi desenhado a parte para evitar perda de tempo sem saber como mostrar as informações de maneira minimamente agradável na tela e se encontra na pasta `<root>/design` em arquivo SVG feito no software gratuito [Inkscape](https://inkscape.org/);

Para roteamento foi escolhido o React-router por ser simples de configurar para um projeto pequeno como este e oferecer as ferramentas necessárias para o proposto. Apenas uma rota de página não encontrada. Existe uma página no repositório, que fazia parte do fluxo pensado para a implementação inicial, porém, o meu tempo extra foi pouco para realizar uma implementação completa como desejava.

Foi utilizado styled-components por ser uma biblioteca consolidada no mercado front-end e por sua flexibilidade e simplicidade na criação das folhas de estilo junto ao código.

## Fluxo

O fluxo compreende apenas uma tela onde os dados são buscados da API construída integrando-se com uma outra API responsável por entregar os dados da SpaceX sobre os próximos lançamentos, os já executados até então o último e o próximo. Para evitar um atraso muito grande no carregamento da página pela quantidadede dados foi adicionado um mecanismo de cache no back-end com o tempo apropriado para acompanhar de maneira aproximada o cache fixado na API Rest Consumida e também a busca pelos dados foi comandada de maneira assíncrona, onde os menores payloads esperados vêm primeiro para começar a preencher a tela enquanto os restantes seguem seu fluxo sem impedir que o usuário tenha algo para visualizar.
