import { H1, PageWrapper, Row } from "../../components/commons/commons";

export const NoTFound: React.FC = () => (
    <PageWrapper>
        <Row>
            <H1>Sorry, page not found ;(</H1>
        </Row>
    </PageWrapper>
);
