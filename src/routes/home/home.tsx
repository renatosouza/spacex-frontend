import { useEffect, useState } from "react";
import { ContentSection, H1, PageWrapper, Row, TextButton } from "../../components/commons/commons";
import { Maincard } from "../../components/main-card/main-card";
import { MinimalCard } from "../../components/minimal-card/minimal-card";
import { YoutubeCard } from "../../components/you-tube-card/youtube-card";
import { Launch } from "../../entities/Launch";
import {
    getLatestLaunches,
    getNextLaunches,
    getPastLaunches,
    getUpcomingLaunches
} from "../../resources/api";

export const Home: React.FC = () => {
    const [latest, setLatest] = useState<Launch>();
    const [next, setNext] = useState<Launch>();
    const [upcomings, setUpcomings] = useState<Launch[]>();
    const [olders, setOlders] = useState<Launch[]>();

    useEffect(() => {
        async function getData() {
            const lat = await getLatestLaunches();
            if (!lat.error) {
                setLatest(lat.data);
            }

            const nxt = await getNextLaunches();
            if (!nxt.error) {
                setNext(nxt.data);
            }

            const upcom = await getUpcomingLaunches();
            if (!upcom.error) {
                setUpcomings(upcom.data);
            }

            const old = await getPastLaunches();
            if (!old.error) {
                setOlders(old.data);
            }
        }

        getData();

        return function cleanup() {
            setLatest(undefined);
            setNext(undefined);
            setUpcomings(undefined);
            setOlders(undefined);
        };
    }, []);

    return (
        <PageWrapper>
            <H1>Prepare for Next Launch</H1>
            <div style={{ marginTop: "3rem" }}>
                <Maincard
                    key={next?.rocket}
                    title={next?.name}
                    description={next?.dateLocal}
                    width={window.screen.width * 0.5 + "px"}
                    height='230px'
                    img={next?.largeImages[0] || next?.smallImages[0]}
                />
            </div>

            <ContentSection>
                <h2>Latest Launches</h2>
                <Row style={{ marginTop: "1rem" }}>{chooseCard(latest)}</Row>
            </ContentSection>

            <ContentSection>
                <h2>Upcoming Launches</h2>
                <Row style={{ marginTop: "1rem", flexWrap: "wrap" }}>
                    {getMinimalCard(upcomings)}
                </Row>
            </ContentSection>

            <ContentSection>
                <h2>past Launches</h2>
                <Row style={{ marginTop: "1rem", flexWrap: "wrap" }}>{getMinimalCard(olders)}</Row>
            </ContentSection>
        </PageWrapper>
    );
};
function getMinimalCard(upcomings: Launch[] | undefined) {
    return upcomings?.map((item) => (
        <MinimalCard
            key={item.rocket}
            title={item.name}
            description={item.dateLocal}
            rocket={item.flightNumber.toString()}
            width='65px'
            height='65px'
            img={item?.largeImages[0] || item?.smallImages[0]}
        />
    ));
}

function chooseCard(latest: Launch | undefined) {
    return latest?.youtubeId ? (
        <YoutubeCard
            key={latest?.rocket}
            title={latest?.name}
            description={latest?.dateLocal}
            width='260px'
            height='180px'
            youtubeId={latest?.youtubeId}
        />
    ) : (
        <Maincard
            key={latest?.rocket}
            title={latest?.name}
            description={latest?.dateLocal}
            width='260px'
            height='180px'
            img={latest?.largeImages[0] || latest?.smallImages[0]}
        />
    );
}
