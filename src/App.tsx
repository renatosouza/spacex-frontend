import { BaseLayout } from "./components/layout/BaseLayout";
import styled from "styled-components";
import theme from "./components/theme";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Home } from "./routes/home/home";
import { Details } from "./routes/details/details";
import { NoTFound } from "./routes/not-found/not-found";

const AppContainer = styled.main`
    background-color: ${theme.colors.background};
    min-height: 100vh;
`;

function App() {
    return (
        <AppContainer className='App'>
            <BaseLayout>
                <BrowserRouter>
                    <Routes>
                        <Route path='/' element={<Home />}></Route>
                        <Route path='/details' element={<Details />}></Route>
                        <Route path='/*' element={<NoTFound />}></Route>
                    </Routes>
                </BrowserRouter>
            </BaseLayout>
        </AppContainer>
    );
}

export default App;
