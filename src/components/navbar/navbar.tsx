import React from "react";
import styled from "styled-components";
import theme from "../theme";

const Nav = styled.nav`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
`;

const Subtitle = styled.h2`
    color: ${theme.colors.lightColor};
    font-weight: 600;
    padding-left: 0.3rem;
    font-size: 18pt;
`;

const Title = styled(Subtitle)`
    color: ${theme.colors.baseTextColor};
`;

const BrandBox = styled.div`
    display: flex;
    align-items: baseline;
    justify-content: flex-start;
`;

export type Props = {
    title: string;
    subtitle?: string;
};

const Navbar: React.FC<Props> = ({ title, subtitle, children }) => (
    <Nav>
        <BrandBox>
            <Title>{title}</Title>
            <Subtitle>{subtitle}</Subtitle>
        </BrandBox>
        <div>{children}</div>
    </Nav>
);

export default Navbar;
