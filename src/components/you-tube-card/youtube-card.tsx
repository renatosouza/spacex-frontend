import styled from "styled-components";
import theme from "../theme";
import { SkeletonAnimation } from "../commons/skeleton";
import { YTFrame } from "../ytframe/ytframe";

const CardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    margin: 0 1.5rem 0 0;

    &:hover {
        cursor: pointer;
    }
`;

const CardDescription = styled.h5`
    color: ${theme.colors.lightColor};
`;

type CardProps = {
    youtubeId: string;
    title?: string;
    description?: string;
    width: string;
    height: string;
};

export const YoutubeCard: React.FC<CardProps> = ({
    youtubeId,
    title,
    description,
    width,
    height
}) =>
    !youtubeId && !description && !title ? (
        <div>
            <SkeletonAnimation width={width} height={height} borderRadius='1rem' />
            <SkeletonAnimation width='90px' height='30px' borderRadius='.4rem' />
            <SkeletonAnimation width='120px' height='20px' borderRadius='.4rem' />
        </div>
    ) : (
        <CardWrapper>
            <YTFrame width={width} height={height} youtubeVideoId={youtubeId} />
            <h3>{title}</h3>
            <CardDescription>{description}</CardDescription>
        </CardWrapper>
    );
