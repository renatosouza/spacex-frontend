import styled from "styled-components";
import { CardImageWrapper } from "../commons/card-commons";
import { PrimaryText, Shaddowed } from "../commons/commons";
import { SkeletonAnimation } from "../commons/skeleton";
import theme from "../theme";

const MinimalCardWrapper = styled(Shaddowed)`
    display: flex;
    align-items: center;
    background-color: white;
    border-radius: 0.5rem;
    position: relative;
    margin: 1rem 1rem 1rem 0;
    padding: 0.4rem;
    min-width: 320px;

    &:hover {
        cursor: pointer;
    }
`;

const CardDescription = styled.h6`
    color: ${theme.colors.lightColor};
    white-space: nowrap;
    padding-right: 1rem;
`;

type CardProps = {
    img?: string;
    title?: string;
    description?: string;
    rocket?: string;
    width?: string;
    height?: string;
};

export const MinimalCard: React.FC<CardProps> = ({
    img,
    title,
    description,
    rocket,
    width,
    height
}) =>
    !img && !title && !description ? (
        <div>
            <SkeletonAnimation width='320px' height='80px' borderRadius='.5rem' />
        </div>
    ) : (
        <MinimalCardWrapper>
            <CardImageWrapper width={width} height={height} img={img} />
            <div style={{ margin: "0 1rem" }}>
                <h5 title={title} style={{ textOverflow: "ellipsis" }}>
                    {title?.substring(0, 23)}
                </h5>
                <CardDescription>{description}</CardDescription>
            </div>
            <PrimaryText>
                <h3>{rocket}</h3>
            </PrimaryText>
        </MinimalCardWrapper>
    );
