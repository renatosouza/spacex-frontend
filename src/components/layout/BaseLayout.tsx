import styled from "styled-components";
import theme from "../theme";
import Navbar from "../navbar/navbar";
import { Footer } from "../footer/footer";

const Main = styled.main`
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin: auto;
    max-width: ${theme.measures.containerMaxWidth};
    min-width: ${theme.measures.containerMinWidth};
    padding: 2rem;

    @media (max-width: 800px) {
        padding: 0.5rem;
    }

    font-family: Poppins, Arial, Helvetica, sans-serif;
`;

export const BaseLayout: React.FC = ({ children }) => (
    <Main>
        <Navbar title='SpaceX' subtitle='Launches' />
        {children}
        <Footer>
            maded with{" "}
            <span style={{ color: theme.colors.mainColor, margin: "0 .4rem" }}>&#x2764;</span> by
            <em>Renato Souza</em> - renatosdesign@gmail.com
        </Footer>
    </Main>
);
