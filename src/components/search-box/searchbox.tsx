import styled from "styled-components";
import theme from "../theme";

export const SearchInput = styled.input`
    border: none;
    padding: 0.4rem;
    outline: none;
    color: ${theme.colors.lightColor};
    font-size: 10pt;
    font-weight: 300;
    width: 100%;
`;

export const SearchIcon = styled.div`
    width: 30px;
    height: 30px;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;

    &:before {
        content: "";
        position: absolute;
        width: 14px;
        height: 14px;
        border-radius: 15px;
        border: solid 2.5px #e6c997ff;
    }

    &::after {
        content: "";
        top: 0;
        bottom: 0;
        width: 2px;
        height: 6px;
        background-color: #e6c997ff;
        border-radius: 4px;
        position: absolute;
        transform: translate(-414%, 53%) rotate(136deg);
    }

    transform: rotate(180deg);
`;

export const SearchWrapper = styled.div`
    padding: 1rem 4rem;
    border-radius: 4rem;
    background-color: white;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

export const SearchBox: React.FC = () => (
    <SearchWrapper>
        <SearchIcon />
        <SearchInput placeholder='Search by name or number' type='search' />
    </SearchWrapper>
);
