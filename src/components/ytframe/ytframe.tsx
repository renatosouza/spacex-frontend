import styled from "styled-components";

type YTFrameProps = {
    width: string;
    height: string;
    youtubeVideoId: string;
};

const IFrame = styled.iframe`
    width: ${(props: Partial<YTFrameProps>) => props.width};
    height: ${(props: Partial<YTFrameProps>) => props.height};
`;

export const YTFrame: React.FC<YTFrameProps> = ({ width, height, youtubeVideoId }) => (
    <IFrame
        width={width}
        height={height}
        src={"https://www.youtube.com/embed/" + youtubeVideoId}
        title='YouTube video player'
        allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'></IFrame>
);
