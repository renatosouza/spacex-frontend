import styled from "styled-components";
import theme from "../theme";

const StyledFooter = styled.footer`
    display: flex;
    width: 100%;
    padding: 2rem 1rem;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    color: ${theme.colors.lightColor};
`;

export const Footer: React.FC = ({ children }) => <StyledFooter>{children}</StyledFooter>;
