import styled from "styled-components";
import theme from "../theme";
import { CardImageWrapper } from "../commons/card-commons";
import { baseShaddowedStyle } from "../commons/commons";
import { SkeletonAnimation } from "../commons/skeleton";

const CardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    margin: 0 1.5rem 0 0;

    &:hover {
        cursor: pointer;
    }
`;

const CardDescription = styled.h5`
    color: ${theme.colors.lightColor};
`;

const ImageWrapper = styled(CardImageWrapper)`
    ${baseShaddowedStyle}

    @media (max-width: 800px) {
        width: 100%;
    }
`;

type CardProps = {
    img?: string;
    title?: string;
    description?: string;
    width?: string;
    height?: string;
};

export const Maincard: React.FC<CardProps> = ({ img, title, description, width, height }) =>
    !img && !description && !title ? (
        <div>
            <SkeletonAnimation width={width} height={height} borderRadius='1rem' />
            <SkeletonAnimation width='90px' height='30px' borderRadius='.4rem' />
            <SkeletonAnimation width='120px' height='20px' borderRadius='.4rem' />
        </div>
    ) : (
        <CardWrapper>
            <ImageWrapper img={img} width={width} height={height} />
            <h3>{title}</h3>
            <CardDescription>{description}</CardDescription>
        </CardWrapper>
    );
