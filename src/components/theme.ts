export const colors = {
    background: "#f2f2f2ff",
    mainColor: "#ab37c8ff",
    lightColor: "#9b9b9b",
    baseTextColor: "#1a1a1aff",
    ocre: "#e6c997ff"
};

const measures = {
    paddingButtons: ".3rem",
    marginButtons: ".3rem",
    containerMaxWidth: "1024px",
    containerMinWidth: "320px"
};

export default {
    colors,
    measures
};
