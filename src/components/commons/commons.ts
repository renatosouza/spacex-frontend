import styled, { css } from "styled-components";
import theme from "../theme";

export const H1 = styled.h1`
    font-size: 36pt;
    font-weight: bold;
`;

export const Row = styled.div`
    display: flex;
`;

export const baseShaddowedStyle = css`
    box-shadow: 0 31px 35px rgb(0 0 0 / 9%);
`;

export const Shaddowed = styled.div`
    ${baseShaddowedStyle}
`;

export const PrimaryText = styled.span`
    color: ${theme.colors.mainColor};
`;

export const PageWrapper = styled.section`
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    padding: 5rem 0;
`;

export const ContentSection = styled(PageWrapper)`
    align-items: flex-start;
    padding: 2rem 0;
    max-height: 80vh;
    overflow: auto;
`;

export const TextButton = styled.button`
    padding: ${theme.measures.paddingButtons};
    color: ${theme.colors.mainColor};
    border: none;
    background: transparent;
    outline: none;
    font-size: 14pt;
    font-weight: bold;
    margin-top: 1.5rem;

    &:hover {
        filter: invert(0.3);
        cursor: pointer;
    }
`;
