import styled from "styled-components";

export type CardImageWrapperProps = {
    width?: string;
    height?: string;
    img?: string;
    color?: string;
};

export const CardImageWrapper = styled.div`
    width: ${(props: CardImageWrapperProps) => props.width};
    height: ${(props: CardImageWrapperProps) => props.height};
    background-color: ${(props: CardImageWrapperProps) => props.color || "white"};
    background-image: ${(props: CardImageWrapperProps) => `url('${props.img}')`};
    background-size: cover;
    background-position: center;
    border-radius: 0.5rem;
    position: relative;
    margin: 1rem 0;
`;
