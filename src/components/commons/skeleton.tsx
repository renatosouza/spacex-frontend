import styled from "styled-components";
type SkeletonAnimationProps = { width?: string; height?: string; borderRadius?: string };

export const SkeletonAnimation = styled.div`
    width: ${(props: SkeletonAnimationProps) => props.width};
    height: ${(props: SkeletonAnimationProps) => props.height};
    border-radius: ${(props: SkeletonAnimationProps) => props.borderRadius};
    margin: 0.3rem;

    background: linear-gradient(270deg, #b6b6b6, #ffffff);
    background-size: 200% 200%;

    -webkit-animation: skeleton 0.6s ease infinite;
    -moz-animation: skeleton 0.6s ease infinite;
    animation: skeleton 0.6s ease infinite;

    @-webkit-keyframes skeleton {
        0% {
            background-position: 0% 51%;
        }
        50% {
            background-position: 100% 50%;
        }
        100% {
            background-position: 0% 51%;
        }
    }
    @-moz-keyframes skeleton {
        0% {
            background-position: 0% 51%;
        }
        50% {
            background-position: 100% 50%;
        }
        100% {
            background-position: 0% 51%;
        }
    }
    @keyframes skeleton {
        0% {
            background-position: 0% 51%;
        }
        50% {
            background-position: 100% 50%;
        }
        100% {
            background-position: 0% 51%;
        }
    }
`;
