import axios from "axios";
import { Launch } from "../entities/Launch";

const API_HOST = "http://localhost:3100";
const LAUNCHES_BASEPATH = "/launches";
const apiPaths = {
    nextLaunches: "/next",
    pastLaunches: "/past",
    latestLaunches: "/latest",
    upcomingLaunches: "/upcoming"
};

const http = axios.create({
    baseURL: `${API_HOST}${LAUNCHES_BASEPATH}`
});

export function getNextLaunches() {
    return handleRequests<Launch>(apiPaths.nextLaunches);
}
export function getLatestLaunches() {
    return handleRequests<Launch>(apiPaths.latestLaunches);
}
export function getPastLaunches() {
    return handleRequests<Launch[]>(apiPaths.pastLaunches);
}
export function getUpcomingLaunches() {
    return handleRequests<Launch[]>(apiPaths.upcomingLaunches);
}

async function handleRequests<T>(path: string) {
    try {
        const response = await http.get<T>(path);
        return { error: false, data: response.data };
    } catch (error) {
        return { error: true, data: undefined };
    }
}
