export interface Launch {
    name: string;
    rocket: string;
    dateLocal: string;
    flightNumber: number;
    smallImages: string[];
    largeImages: string[];
    youtubeId: string;
}
